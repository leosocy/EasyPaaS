docker run -it --privileged \
  --net="host" \
  -e MESOS_IP=192.168.1.212 \
  -e MESOS_PORT=5051 \
  -e MESOS_HOSTNAME=192.168.1.212 \
  -e MESOS_MASTER=zk://192.168.1.201:2181,192.168.1.202:2181,192.168.1.203:2181/mesos \
  -e MESOS_WORK_DIR=/var/mesos-slave/work \
  -e MESOS_LOG_DIR=/var/mesos-master/log \
  -e MESOS_DOCKER_MESOS_IMAGE=leosocy/mesos-slave:1.2.0 \
  -e MESOS_SYSTEMD_ENABLE_SUPPORT=false \
  -v /var/mesos-slave/log:/var/mesos-slave/log \
  -v /var/mesos-slave/work:/var/mesos-slave/work \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /cgroup:/cgroup \
  -v /sys/fs/cgroup:/sys/fs/cgroup \
  leosocy/mesos-slave:1.2.0
