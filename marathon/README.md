docker run -it --privileged \
  --net="host" \
  leosocy/marathon:1.4.3 \
  marathon \
  --master zk://192.168.1.201:2181,192.168.1.202:2181,192.168.1.203:2181/mesos \
  --zk zk://192.168.1.201:2181,192.168.1.202:2181,192.168.1.203:2181/marathon
