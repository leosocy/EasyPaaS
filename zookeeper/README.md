On host #1

  docker run -d \
  --net="host" \
  -e SERVER_ID=1 \
  -e ADDITIONAL_ZOOKEEPER_1=server.1=${HOST_IP_1}:2888:3888 \
  -e ADDITIONAL_ZOOKEEPER_2=server.2=${HOST_IP_2}:2888:3888 \
  -e ADDITIONAL_ZOOKEEPER_3=server.3=${HOST_IP_3}:2888:3888 \
  leosocy/zookeeper:3.4.10

On host #2

  docker run -d \
  --net="host" \
  -e SERVER_ID=2 \
  -e ADDITIONAL_ZOOKEEPER_1=server.1=${HOST_IP_1}:2888:3888 \
  -e ADDITIONAL_ZOOKEEPER_2=server.2=${HOST_IP_2}:2888:3888 \
  -e ADDITIONAL_ZOOKEEPER_3=server.3=${HOST_IP_3}:2888:3888 \
   leosocy/zookeeper:3.4.10

On host #3

  docker run -d \
  --net="host" \
  -e SERVER_ID=3 \
  -e ADDITIONAL_ZOOKEEPER_1=server.1=${HOST_IP_1}:2888:3888 \
  -e ADDITIONAL_ZOOKEEPER_2=server.2=${HOST_IP_2}:2888:3888 \
  -e ADDITIONAL_ZOOKEEPER_3=server.3=${HOST_IP_3}:2888:3888 \
   leosocy/zookeeper:3.4.10
