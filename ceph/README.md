# ceph

## What

## Why

## How

### osd

```shell
    docker run -d --net=host \
    --name=myosd1 \
    --privileged=true \
    --pid=host \
    -v /etc/ceph:/etc/ceph \
    -v /var/lib/ceph/:/var/lib/ceph \
    -v /dev/:/dev/ \
    -v /mnt/sda2:/var/lib/ceph/osd \
    ceph/daemon osd_directory
```

### mon

```shell
    docker run -d --net=host \
    --name=mon \
    --privileged=true \
    -v /etc/ceph:/etc/ceph \
    -v /var/lib/ceph/:/var/lib/ceph \
    -e MON_IP=192.168.1.222 \
    -e CEPH_PUBLIC_NETWORK=192.168.1.0/24 \
    ceph/daemon mon
```

### mds

```shell
    docker run -d --net=host \
    -v /var/lib/ceph/:/var/lib/ceph/ \
    -v /etc/ceph:/etc/ceph \
    -e CEPHFS_CREATE=1 \
    ceph/daemon mds
```