#!/bin/bash

if [ ! -f "$1" ]
then
	echo "Please provide the configuration path of mesos-master!"
	exit 0
fi

cat $1 | while read line
do
	if [[ ${line} == "MESOS_"* ]]
	then
		echo "export ${line}" >> /etc/profile
	fi
done
source /etc/profile
mesos-master
