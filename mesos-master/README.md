docker run -it --privileged \
  --net="host" \
  -e MESOS_IP=192.168.1.201 \
  -e MESOS_PORT=5050 \
  -e MESOS_ZK=zk://192.168.1.201:2181,192.168.1.202:2181,192.168.1.203:2181/mesos \
  -e MESOS_QUORUM=2 \
  -e MESOS_LOG_DIR=/var/mesos/log \
  -e MESOS_WORK_DIR=/var/mesos/work \
  -e MESOS_HOSTNAME=192.168.1.201 \
  leosocy/mesos-master:1.2.0
